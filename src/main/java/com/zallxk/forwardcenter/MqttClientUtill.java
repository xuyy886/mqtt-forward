package com.zallxk.forwardcenter;

import jdk.internal.org.jline.utils.Log;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;


@Component
public class MqttClientUtill {

    @Autowired
    private MqttConfig mqttConfig;

    public void connectAndSubscribe(String clientId, String topic) throws MqttException {

        String host = mqttConfig.getCenter().getHost();
        String username = mqttConfig.getCenter().getUsername();
        String password = mqttConfig.getCenter().getPassword();

        Log.info("**********注册台网中心******************");
        String url = mqttConfig.getCenter().getRegister()+ "license="+mqttConfig.getCenter().getLience()+"&owner="+mqttConfig.getCenter().getOwner()+"&type="+mqttConfig.getCenter().getType();
        String requestBody ="{\\\"key\\\":\\\"value\\\"}";
        // 创建HttpClient对象
        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            // 创建HttpPost对象
            HttpPost httpPost = new HttpPost(url);
            // 设置请求头
            httpPost.setHeader("Content-type", "application/json");
            // 设置请求体内容
            httpPost.setEntity(new StringEntity(requestBody));

            // 发送Post请求
            try (CloseableHttpResponse response = httpClient.execute(httpPost)) {
                // 获取响应实体
                HttpEntity entity = response.getEntity();
                // 打印响应内容
                System.out.println(EntityUtils.toString(entity));
                JSONObject jsonResponse = new JSONObject(EntityUtils.toString(entity));

                // 获取每个键对应的值
//                String value = jsonResponse.getString("key");
//                System.out.println("Value: " + value);
                Log.info("**********注册台网中心******************"+jsonResponse.get("host")+","+jsonResponse.get("username"));
                host= (String) jsonResponse.get("host");
                username= (String) jsonResponse.get("username");
                password= (String) jsonResponse.get("password");

            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        MqttClient client = new MqttClient(host, clientId);
        MqttConnectOptions options = new MqttConnectOptions();
        options.setUserName(username);
        options.setPassword(password.toCharArray());
        client.connect(options);
        client.subscribe(topic, (topic1, message) -> {
            // 处理接收到的消息
            // 将消息发布到 forward 主题
            System.out.println("***********转发*******forward***");
            publishToForward(topic1, message.getPayload());
        });
    }

    public void publishToForward(String topic, byte[] payload) throws MqttException {
        String host = mqttConfig.getForward().getHost();
        String username = mqttConfig.getForward().getUsername();
        String password = mqttConfig.getForward().getPassword();

        MqttClient client = new MqttClient(host, MqttClient.generateClientId());
        MqttConnectOptions options = new MqttConnectOptions();
        options.setUserName(username);
        options.setPassword(password.toCharArray());
        client.connect(options);
        client.publish(topic, new MqttMessage(payload));
        client.disconnect();
    }
}
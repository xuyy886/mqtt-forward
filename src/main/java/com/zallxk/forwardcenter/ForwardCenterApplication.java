package com.zallxk.forwardcenter;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;


@SpringBootApplication
public class ForwardCenterApplication {

    public static void main(String[] args) {
//        SpringApplication.run(ForwardCenterApplication.class, args);
        ConfigurableApplicationContext context = SpringApplication.run(ForwardCenterApplication.class, args);

//        // 遍历所有已注入的对象
//        for (String beanName : context.getBeanDefinitionNames()) {
//            Object bean = context.getBean(beanName);
//            System.out.println("Bean Name: " + beanName + ", Bean Class: " + bean.getClass().getName());
//        }

        MqttClientUtill mqttClientUtill = context.getBean(MqttClientUtill.class);
        System.out.println("***********mqtt**********");
        try {
            mqttClientUtill.connectAndSubscribe("earth123456","earthquake");
        } catch (MqttException e) {
            System.out.println("***********mqtt*******22222***");
            throw new RuntimeException(e);
        }

    }

}

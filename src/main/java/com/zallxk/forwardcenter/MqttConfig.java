package com.zallxk.forwardcenter;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "mqtt")
public class MqttConfig {
    private MqttProperties center;
    private MqttProperties forward;

    // Getters and setters


    public MqttProperties getCenter() {
        return center;
    }

    public void setCenter(MqttProperties center) {
        this.center = center;
    }

    public MqttProperties getForward() {
        return forward;
    }

    public void setForward(MqttProperties forward) {
        this.forward = forward;
    }

    public static class MqttProperties {
        private String host;
        private String username;
        private String password;

        private String register;
        private  String lience;
        private String owner;
        private int type;
        // Getters and setters


        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public String getRegister() {
            return register;
        }

        public void setRegister(String register) {
            this.register = register;
        }

        public String getLience() {
            return lience;
        }

        public void setLience(String lience) {
            this.lience = lience;
        }

        public String getOwner() {
            return owner;
        }

        public void setOwner(String owner) {
            this.owner = owner;
        }

        public String getHost() {
            return host;
        }

        public void setHost(String host) {
            this.host = host;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }
    }
}